import static java.lang.Thread.sleep;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Exchanger;

/**
 * User: jim
 * Date: Aug 7, 2008
 * Time: 10:21:48 PM
 * <p/>
 * updated to use 1 timer for gobs of sitting ducks
 * <p/>
 * updated to show unordered queue
 *
 * duck duck goose simulation to leverage java concurrent exchanger.
 *
 * creates 1 polling timer.
 * creates 1 DuckExchanger.
 * creates 1 duck server timer on 2 second interval as Exchanger master syncronizer
 * creates 22 duck clients
 * 22 (maybe 23) immutable duck refs are created and never destroyed, simulating a resource pool for a collection of cores
 *
 * 1 thread is created and creates a 2 second timer.
 * main thread creates a polling timer then waits
 *
 * duck clients exchange a duck with the duck server.
 * exchanger blocks all duck clients on 2 second firing intervals
 * random client timer distribution removes cyclical ordering of exchange.
 *
 * all 22 polling timer tasks are held in check on the Exchanger's agenda as if contention.
 *
 * <code>
 * in the real world Exchanger<Pair<ByteBuffer,Future>> can exchange a Pair<ByteBuffer,null> for a Pair<null,Future<ByteBuffer>> to effect async operations where the master thread only blocks when no client exchangers have fired.
 * </code>
 */
public class SpinMe {
    private Exchanger<Duck> duckExchanger;
    private int anInt;
    private Duck theDuck;
    public static boolean killSwitch;
    private static final int DUCKSCOUNT = Duck.values().length;
    final Random random = new Random();

  enum Duck {
        duck, duckness, ducker, ducky, ducks, ducksFromHell, duckForYourLife, goose
    }

    public static void main(String... args) throws InterruptedException {

      SpinMe spinMe = new SpinMe();
      synchronized (spinMe){
        spinMe.wait();
      }

    }

    public SpinMe() {
        random.setSeed(12341234);

        duckExchanger = new Exchanger<Duck>() {

        };

        Runnable standing = new Runnable() {
            public void run() {
                while (!killSwitch) {
                    int i = random.nextInt();
                    anInt = Math.abs(i) % (DUCKSCOUNT);
                    theDuck = Duck.values()[anInt];
                    try {
                        Duck baggage = duckExchanger.exchange(theDuck);
                        if (Duck.goose == baggage) {
                            System.err.println("GOOSE!@!");
                        }
                        sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
      Thread thread = new Thread(standing);
        thread.start();
      Timer timer = new Timer();

         for (int i = 0; i < 23; i++) {
            long l = Math.abs(random.nextLong() % 5000);
            SittingDuck duck = new SittingDuck(i, l);


            //timer[Task] resists cancelling and restarting so
            // the best we can do for random activation is on the freq
            timer.scheduleAtFixedRate(duck, 0, l);

            System.out.println("Duck#" + i + " freq  " + l);
        }
    }

    private class SittingDuck extends TimerTask {
        private int brand;

        Duck duck = null;
      final private long interval;

        SittingDuck(int brand, long interval) {this.brand = brand;
          this.interval = interval;
        }

        public void run() {

            try {
                duck = duckExchanger.exchange(duck);
                if (null != duck) {
                    System.out.println("timertask #" + brand + " returns -> " + duck+ " interval: ("+interval+")");
                } else
                    System.out.println("timertask #" + brand + " Null!  Bad news!");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }




}